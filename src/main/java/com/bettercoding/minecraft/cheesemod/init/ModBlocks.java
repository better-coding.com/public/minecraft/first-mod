package com.bettercoding.minecraft.cheesemod.init;


import com.bettercoding.minecraft.cheesemod.Reference;
import com.bettercoding.minecraft.cheesemod.block.CheeseBlock;
import net.minecraft.block.Block;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Mod.EventBusSubscriber(modid = Reference.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModBlocks {
    private static final Logger LOGGER = LogManager.getLogger();
    public static Block cheese_block;

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        LOGGER.info("registerBlocks");
        cheese_block = new CheeseBlock();
        event.getRegistry().register(cheese_block);
    }
}

