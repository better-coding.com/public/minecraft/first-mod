package com.bettercoding.minecraft.cheesemod.init;

import com.bettercoding.minecraft.cheesemod.Reference;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod.EventBusSubscriber(modid = Reference.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModItems {
    private static final Logger LOGGER = LogManager.getLogger();

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        LOGGER.info("registerItems");
        Item cheeseBlockItem = createItemBlockForBlock(ModBlocks.cheese_block, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS).maxStackSize(64));
        event.getRegistry().register(cheeseBlockItem);
    }

    private static ItemBlock createItemBlockForBlock(Block block, Item.Properties properties) {
        return (ItemBlock) new ItemBlock(block, properties).setRegistryName(block.getRegistryName()+"_item");
    }
}

