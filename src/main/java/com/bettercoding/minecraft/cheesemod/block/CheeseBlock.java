package com.bettercoding.minecraft.cheesemod.block;

import com.bettercoding.minecraft.cheesemod.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class CheeseBlock extends Block {

    public static final String BLOCK_NAME = "cheese_block";

    public CheeseBlock() {
        super(createProperties());
        setRegistryName(Reference.MODID, BLOCK_NAME);
    }

    private static Properties createProperties() {
        return Properties.create(Material.ROCK).hardnessAndResistance(3.0f, 3.0f);
    }
}
